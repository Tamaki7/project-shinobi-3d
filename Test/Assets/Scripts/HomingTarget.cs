using UnityEngine;

public class HomingTarget : MonoBehaviour
{
    public float moveSpeed = 5f;

    private Rigidbody rb;
    private Transform target;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        target = GameObject.FindWithTag("Player").transform;
    }

    void FixedUpdate()
    {
        // Mueve el objetivo hacia el jugador
        Vector3 direction = (target.position - transform.position).normalized;
        rb.velocity = direction * moveSpeed;
    }

    void OnTriggerEnter(Collider other)
    {
        // Si el objetivo colisiona con el jugador, se destruye
        if (other.CompareTag("Player"))
        {
            Destroy(gameObject);
        }
    }
}